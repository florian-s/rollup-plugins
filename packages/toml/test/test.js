const { readFileSync } = require('fs');

const test = require('ava');
const { rollup } = require('rollup');

const { nodeResolve } = require('@rollup/plugin-node-resolve');

const { testBundle } = require('../../../util/test');

const toml = require('..'); // eslint-disable-line import/no-unresolved

const read = (file) => readFileSync(file, 'utf-8');

require('source-map-support').install();

process.chdir(__dirname);

test('converts TOML', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/basic/main.js',
    plugins: [toml()]
  });
  t.plan(1);
  return testBundle(t, bundle);
});

test('handles arrays', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/array/main.js',
    plugins: [toml()]
  });
  t.plan(1);
  return testBundle(t, bundle);
});

test('handles literals', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/literal/main.js',
    plugins: [toml()]
  });
  t.plan(1);
  return testBundle(t, bundle);
});

test('handles simple comment', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/comments/main.js',
    plugins: [toml()]
  });
  t.plan(1);
  return testBundle(t, bundle);
});

test('generates named exports', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/named/main.js',
    plugins: [toml()]
  });

  const { code, result } = await testBundle(t, bundle, { exports: {} });

  t.is(result.version, '1.33.7');
  t.is(code.indexOf('this-should-be-excluded'), -1, 'should exclude unused properties');
});

test('resolves extensionless imports in conjunction with the node-resolve plugin', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/extensionless/main.js',
    plugins: [nodeResolve({ extensions: ['.js', '.toml'] }), toml()]
  });
  t.plan(2);
  return testBundle(t, bundle);
});

test('handles toml objects with no valid keys (#19)', async (t) => {
  const bundle = await rollup({
    input: 'fixtures/no-valid-keys/main.js',
    plugins: [toml()]
  });
  t.plan(1);
  return testBundle(t, bundle);
});

test('handles garbage', async (t) => {
  const warns = [];

  await rollup({
    input: 'fixtures/garbage/main.js',
    plugins: [toml()],
    onwarn: (warning) => warns.push(warning)
  }).catch(() => {});

  const [{ message, id, position, plugin }] = warns;

  t.is(warns.length, 1);
  t.is(plugin, 'toml');
  t.is(position, 1);
  t.is(message, 'Could not parse TOML file');
  t.regex(id, /(.*)bad.toml$/);
});

test('does not generate an AST', async (t) => {
  // eslint-disable-next-line no-undefined
  t.is(toml().transform(read('fixtures/form/input.toml'), 'input.toml').ast, undefined);
});

test('does not generate source maps', async (t) => {
  t.deepEqual(toml().transform(read('fixtures/form/input.toml'), 'input.toml').map, {
    mappings: ''
  });
});

test('generates properly formatted code', async (t) => {
  const { code } = toml().transform(read('fixtures/form/input.toml'), 'input.toml');
  t.snapshot(code);
});

test('generates correct code with preferConst', async (t) => {
  const { code } = toml({ preferConst: true }).transform(
    read('fixtures/form/input.toml'),
    'input.toml'
  );
  t.snapshot(code);
});

test('uses custom indent string', async (t) => {
  const { code } = toml({ indent: '  ' }).transform(read('fixtures/form/input.toml'), 'input.toml');
  t.snapshot(code);
});

test('generates correct code with compact=true', async (t) => {
  const { code } = toml({ compact: true }).transform(
    read('fixtures/form/input.toml'),
    'input.toml'
  );
  t.snapshot(code);
});

test('generates correct code with namedExports=false', async (t) => {
  const { code } = toml({ namedExports: false }).transform(
    read('fixtures/form/input.toml'),
    'input.toml'
  );
  t.snapshot(code);
});

test('correctly formats arrays with compact=true', async (t) => {
  t.deepEqual(
    toml({ compact: true, namedExports: false }).transform(
      `test = [
  1,
  { "x" = 1 }
]`,
      'input.toml'
    ).code,
    'export default{test:[1,{x:1}]};'
  );
});
