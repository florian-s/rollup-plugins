import mimeDb from './mime-db.toml';

t.deepEqual(mimeDb['application/1d-interleaved-parityfec'], {
  source: 'iana'
});
