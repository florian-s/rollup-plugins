import { createFilter, dataToEsm } from '@rollup/pluginutils';
import jsToml from '@ltd/j-toml';

export default function toml(options = {}) {
  const filter = createFilter(options.include, options.exclude);
  const indent = 'indent' in options ? options.indent : '\t';

  return {
    name: 'toml',

    // eslint-disable-next-line no-shadow
    transform(tomlSrc, id) {
      if (id.slice(-5) !== '.toml' || !filter(id)) return null;

      try {
        const parsed = jsToml.parse(tomlSrc, { bigint: false });
        return {
          code: dataToEsm(parsed, {
            preferConst: options.preferConst,
            compact: options.compact,
            namedExports: options.namedExports,
            indent
          }),
          map: { mappings: '' }
        };
      } catch (err) {
        const message = 'Could not parse TOML file';
        // TODO : find the real position
        const position = 1;
        this.warn({ message, id, position });
        return null;
      }
    }
  };
}
