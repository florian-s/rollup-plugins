# @rollup/plugin-toml ChangeLog

## 0.1.1

_2022-04-10_

- Update README for npm

## 0.1.0

_2022-04-10_

- First release
